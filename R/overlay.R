
.kabsch.algo <- function(X, Y, allow.improper = FALSE){
    n <- ncol(X)
    C   <- X %*% t(Y)
    SVD <- svd(C)
    v <- SVD$v
    tu <- t(SVD$u)
    s <- SVD$d
    d <- if (allow.improper){ 1 }else{sign(det(v %*% tu )) }
    D   <- diag(c(1, 1, d))
    rotation <- v %*% D %*% tu
    e <- (sum(X^2) + sum(Y^2))/n  - (s[1] + s[2] + (s[3]*d) ) * 2 / n
    result <- list()
    result[["rmsd"]] <- if(e < 0 ){0}else{sqrt(e)}
    result[["rotation"]] <- rotation
    result
}


move_rigidly <- function(xyz, move){
#    center <- xyz %>% dplyr::select(X, Y, Z) %>% colMeans
    centered <- xyz %>% dplyr::mutate(X = X - move$center[1],
                                      Y = Y - move$center[2],
                                      Z = Z - move$center[3]) %>%
        dplyr::select(X, Y, Z) %>% as.matrix() %>% t() 
    new <- move$rotation %*% centered
    xyz %>% dplyr::mutate(X = new[1,] + move$center[1] + move$translation[1],
                          Y = new[2,] + move$center[2] + move$translation[2],
                          Z = new[3,] + move$center[3] + move$translation[3])
}

overlay <- function(xyz, target, value = "rmsd", allow.improper = FALSE){

    c1 <- xyz %>% dplyr::select(X, Y, Z) %>% colMeans

    xyz.c <- xyz %>% dplyr::mutate(X = X - c1["X"],
                                   Y = Y - c1["Y"],
                                   Z = Z - c1["Z"])

    c2 <- target %>% dplyr::select(X, Y, Z) %>% colMeans

    target.c <- target %>% dplyr::mutate(X = X - c2["X"],
                                         Y = Y - c2["Y"],
                                         Z = Z - c2["Z"])

    r <- .kabsch.algo(xyz.c    %>% dplyr::select(X, Y, Z) %>% as.matrix() %>% t(),
                      target.c %>% dplyr::select(X, Y, Z) %>% as.matrix() %>% t(),
                      allow.improper)
    move <- list(translation = c2 - c1,
                 rotation = r$rotation,
                 center = c1)
    if (value == "rmsd")
        return(r$rmsd)
    if (value == "move")
        return( move )
    if (value == "overlay")
        return( move_rigidly(xyz, move))
    stop("Value should be one of 'rmsd', 'move' or 'overlay'")
}
