# chemistryr

## Установка
```r
library(devtools)
install.packages("rmarkdown") # to compile vignettes
install_gitlab("dmitrienka/chemistryr", build_vignettes=TRUE)
```

## Использование
```r
library(chemistryr)
vignette("symmetry_number")
```

