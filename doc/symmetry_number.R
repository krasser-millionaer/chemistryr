## ---- include = FALSE---------------------------------------------------------
knitr::opts_chunk$set(
  collapse = TRUE,
  comment = "#>"
)

## ----setup--------------------------------------------------------------------
Sys.setenv(RGL_USE_NULL = TRUE) ## don't show rgl x11 windows 
library(chemistryr)
library(dplyr)
library(purrr)
rgl::setupKnitr()

## -----------------------------------------------------------------------------
cu12.path  <- system.file("extdata", "Fe12-Cu12.xyz", package="chemistryr")
cu12 <- read_xyz(cu12.path)
cu12

## -----------------------------------------------------------------------------
cu20 <- system.file("extdata", "Fe12-Cu20.xyz", package="chemistryr") %>% read_xyz()
cu11 <- system.file("extdata", "Fe12-Cu11_28.xyz", package="chemistryr") %>% read_xyz()

## -----------------------------------------------------------------------------
plot(cu11)
rgl::rglwidget(width=700, height=600) # генерирует html виджет, обычно не нужен

## -----------------------------------------------------------------------------
multiplicity(cu12)

cu12 %>% filter(Element != "H") %>% multiplicity()


## -----------------------------------------------------------------------------
cu12.trimmed  <- cu12 %>% filter(! Element %in% c("C", "H"))
plot(cu12.trimmed)
rgl::rglwidget(width=700, height=600)

## -----------------------------------------------------------------------------
multiplicity(cu12.trimmed)


## -----------------------------------------------------------------------------
cu20 %>% filter(! Element %in% c("C", "H")) %>% multiplicity()

cu11 %>% filter(! Element %in% c("C", "H")) %>% multiplicity()

## -----------------------------------------------------------------------------
cu11 %>% filter(! Element %in% c("C", "H")) %>% multiplicity(value="rmsds")
cu20 %>% filter(! Element %in% c("C", "H")) %>% multiplicity(value="rmsds")

## -----------------------------------------------------------------------------
cu20 %>% filter(! Element %in% c("C", "H")) %>% multiplicity(treshold=0.1)
cu11 %>% filter(! Element %in% c("C", "H")) %>% multiplicity(treshold=0.1)

## -----------------------------------------------------------------------------
my.dir <- system.file("extdata", package="chemistryr") # путь к папке
files <- dir(my.dir, pattern = "[.]xyz$", full.names=T)

sigmas <- map_int(files,
                  ~ .x %>% read_xyz %>%
                    filter(! Element %in% c("C", "H")) %>%
                    multiplicity(treshold=0.1))

tibble(File = basename(files), Sigma = sigmas)


